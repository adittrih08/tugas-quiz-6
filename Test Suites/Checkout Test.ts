<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Checkout Test</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>6c57e5e5-c5ee-43c8-bc73-d336749693cd</testSuiteGuid>
   <testCaseLink>
      <guid>ff996d9a-def8-4429-9202-e6a290050de2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout/TC06-Open product details</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>4d4266d5-c8f0-4b40-bf50-e9c51e3b421f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout/TC07-Add product to cart</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>53d14143-64d2-48d6-a068-ea30411a5376</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout/TC08-Remove backpack produk from cart</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>0f1ffb54-3041-46c6-bb3a-033c217f2a17</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Checkout/TC09-View products that have been added to your cart</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>5579bd1f-dde9-4aa2-a612-58135d600858</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <iterationNameVariable>
         <defaultValue>''</defaultValue>
         <description></description>
         <id>27269250-e4bf-45bb-8467-2c2acdd1dade</id>
         <masked>false</masked>
         <name>firstName</name>
      </iterationNameVariable>
      <testCaseId>Test Cases/Checkout/TC10-Checkout products with data binding</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>6f038971-6738-4566-88e2-d5265ac19ac8</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/testData</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>6f038971-6738-4566-88e2-d5265ac19ac8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>firstName</value>
         <variableId>27269250-e4bf-45bb-8467-2c2acdd1dade</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6f038971-6738-4566-88e2-d5265ac19ac8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>lastName</value>
         <variableId>c3913292-c330-4cda-b0c4-e94045486327</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>6f038971-6738-4566-88e2-d5265ac19ac8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>postalCode</value>
         <variableId>6e7dc743-2836-4049-bbe3-48bbeceb8120</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
